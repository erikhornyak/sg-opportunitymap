var locations = [];
var map;
if (typeof SAFEGUARD == 'undefined') { 
  SAFEGUARD = {};
}

SAFEGUARD.site = function() 
{
	return {

		init: function () 
		{
			console.log('JS Working');
			
			SAFEGUARD.site.initEvents();
		},
		
		initEvents: function ()
		{

			SAFEGUARD.site.jsonLocations();
			
			if( navigator.userAgent.match(/Android/i)){
			  $('body').addClass('android');
			}
			
			SAFEGUARD.site.mobileSwipeIntroScreens();
			SAFEGUARD.site.listViewTrigger();

			$.getJSON('//freegeoip.net/json/', function(data){
				var currentlocation = data.city + ', '+data.region_name+' '+data.zip_code;
				$('#autocomplete-search1').val(currentlocation);
				$('#browser-orders-submit').prop('disabled', false);
			});

			$('#autocomplete-search1').focus(function(){
				if ( $(this).val() != '' ) {
					var locationsearchparent = $(this).parents('.rc-location-search');
					$(this).parents('.rc-location-search').addClass('rc-dropdown-visible');
					setTimeout(function(){
						if (!$('.pac-container').first().is(':visible')) {
							locationsearchparent.removeClass('rc-dropdown-visible');
						}
					}, 10);
				}
			}).blur(function(){
				$(this).parents('.rc-location-search').removeClass('rc-dropdown-visible');
			});
			$('#autocomplete-search1').keyup(function(){
				if ($(this).val() != '') {
					$(this).parents('.rc-location-search').addClass('rc-dropdown-visible');
				} else {

					$(this).parents('.rc-location-search').removeClass('rc-dropdown-visible');
				}

			});
			$('.pac-item').click(function(){
				$('#browser-orders-submit').prop('disabled', false);
			});

			$('.rc-map-container .rc-work-order-drawer .rc-work-order-close-btn').click(function(){

				$('.rc-work-order-drawer').fadeOut();
				if ($(window).width() < 600) {
					$('.rc-map-container .rc-sidebar .rc-btn.rc-filter-btn').fadeIn();
					$('.rc-sidebar-work-order-container').fadeOut();
				}
			});

			if ($.cookie("SGOO-Instructions-Seen") == 'true') {
				$('.rc-modal-step-6').modal({backdrop: 'static'});
			} else {
				$('.rc-modal-step-1').modal({backdrop: 'static'});
				$.cookie("SGOO-Instructions-Seen", 'true', { expires : 10 });
			}
			$('.rc-modal:not(.rc-alt-step-modal) .rc-modal-header .rc-close:not(.rc-skip-intro)').mouseover(function(){
				$(this).addClass('rc-hide-close');
				$(this).parents('.rc-modal-header').find('.rc-skip-intro').addClass('rc-show-close');
			});
			$('.rc-modal .rc-modal-header .rc-close.rc-skip-intro').mouseout(function(){
				$(this).removeClass('rc-show-close');
				$(this).parents('.rc-modal-header').find('.rc-close').not('.rc-skip-intro').removeClass('rc-hide-close');
			});
			SAFEGUARD.site.initAutocomplete();
			SAFEGUARD.site.mapFilter();

			$('#browser-orders-submit').click(function(){
				var currentsearchval = $('#autocomplete-search1').val();
				$('.rc-sidebar-location-value').val(currentsearchval);
				SAFEGUARD.site.getLocationSearchLatLon(currentsearchval);
	        	$('.rc-modal-step-6').modal('toggle');
	        	$('.rc-map-container').animate({'opacity': '1'});

	        });

	        $('.rc-work-order-drawer .rc-btn-primary').click(function(e){
				e.preventDefault();
				var workordertosend = $(this).attr('data-order'); 
				SAFEGUARD.site.SubmitRequests(workordertosend);
			});

			$('.rc-sidebar .rc-btn-search-icon').click(function(){
				if ($(window).width() > 600 ) {
					var newLocationValue = $('#sidebar-location-value1').val();
				} else {
					var currentsearchval = $('#sidebar-location-value2').val();
				}
	        	$('.rc-zoom-in-snackbar').hide();
	        	$('.rc-infowindow-mobile').hide();
	        	SAFEGUARD.site.getLocationSearchLatLon(newLocationValue, 'all');
			});
		},
		googleMapsInitialize2: function(locationsearch, type, zoom, center) {
			$('.rc-list-view-container').html('');
			var typearrays = {
				all : ["M","I","G"],
				GI: ["G","I"],
				GM: ["G","M"],
				IM: ["I","M"],
				G: ["G"],
				I: ["I"],
				M: ["M"],
				none: []
			};
			if (typeof type === "undefined" || type === null ) { 
		    	typefinal = typearrays['all']; 
		  	} else {
		  		typefinal = typearrays[type];
		  	}
		  	
			var markers = [];

		  	var locations2 = [];
		  	for(var i = 0 ; i< locations.length; i++) {
			    var obj = locations[i];
			    if ( $.inArray(obj["WG"], typefinal) > -1 ) {
			    	locations2.push(obj);
			    } else {

			    }

			}
			var stylesArray = [
			  {
			    featureType: 'landscape',
			    elementType: 'geometry',
			    stylers: [
			      {color: '#f2f2f2'},
			    ]
			  },
			  {
			    featureType: 'landscape.natural',
			    elementType: 'geometry',
			    stylers: [
			      {color: '#f2f2f2'},
			    ]
			  },
			  {
		        featureType: "water",
		        elementType: "geometry",
		        stylers: [
		            {color: "#c3d5d5"},
		        ]
		      },
			  {
			    featureType: 'poi',
			    elementType: 'geometry',
			    stylers: [
			      {color: '#eaeaea'},
			    ]
			  },
			  {
			   	featureType: "poi",
			    elementType: "labels",
				stylers: [
			    	{ visibility: "off" }
				]
			  },
			  {
			    featureType: 'poi.park',
			    elementType: 'geometry',
			    stylers: [
			      {color: '#f2f2f2'},
			    ]
			  },
			  {
			    featureType: 'poi.school',
			    elementType: 'geometry',
			    stylers: [
			      {color: '#f2f2f2'},
			    ]
			  },
			  {
			    featureType: 'road.highway',
			    elementType: 'geometry',
			    stylers: [
			      {color: '#e2e2e2'},
			    ]
			  },
			  {
			    featureType: 'transit.station',
			    elementType: 'all',
			    stylers: [
			      { visibility: 'off' }
			    ]
			  },
			];
			var oldicon;
			var draggableValue = true;
		  	var mapzoom = 12;

			if ($(window).width() > 900) {
			  	mapzoom = 12;
			} else if ($(window).width() > 767) {
			  	mapzoom = 15;
			} else if ($(window).width() > 500) {
			  	mapzoom = 15;
			} else {
			 	mapzoom = 16;
			}
			if (zoom != '' && zoom != null) {
		  		mapzoom = zoom;
		  	}
			var mapOptions = {
			    zoom: mapzoom,
			    scrollwheel: true,
			    draggable: draggableValue,
			    streetViewControl: false,   
			    mapTypeControl: false,
			    zoomControl: false,
			    minZoom: 9,
			    maxZoom: 14,
		    }
			map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
			
			var mapTypeControlDiv = document.createElement('div');
		  	var mapTypeControl = new SAFEGUARD.site.mapStyleControl(mapTypeControlDiv, map);
		  	map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(mapTypeControlDiv);
			var zoomControlDiv = document.createElement('div');
		  	var zoomControl = new SAFEGUARD.site.ZoomControl(zoomControlDiv, map);
		  	map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(zoomControlDiv);
			
			var location = locationsearch; //"Cleveland, OH";

			if (center != '' && center != null) {
				map.setCenter(center);
			} else {
				map.setCenter(location);
			}

			var infowindow = null;
			  infowindow = new InfoBubble({
				content: "holding...",
				hideCloseButton: true,
				borderRadius: 4,
		        arrowSize: 15,
		        borderWidth: 0,
		        borderRadius: 5,
		        disableAutoPan: true,
		        hideCloseButton: true,
		        arrowPosition: 15,
		        padding: 0,
		        backgroundClassName: 'info-container',
		        minWidth: 250,
		        minHeight: 125,
		        maxHeight: 300,
			  });
		    var iconBase = '';
		    var marker, i;

			var image = 'Images/inspection-dot.png';
	  		map.setOptions({styles: stylesArray});


	  		for (i = 0; i < locations2.length; i++) {
	  			var header;
	  			var markerimg;
	  			var markerimgselected;
	  			if (locations2[i]['WG'] == 'I') {
	  				markerimg = 'Images/inspection-dot.png';
	  				markerimgselected = 'Images/inspection-dot-selected.png';
	  			} else if (locations2[i]['WG'] == 'G') {
	  				markerimg = 'Images/lawn-dot.png';
	  				markerimgselected = 'Images/lawn-dot-selected.png';
	  			} else if (locations2[i]['WG'] == 'M') {
	  				markerimg = 'Images/maintenance-dot.png';
	  				markerimgselected = 'Images/maintenance-dot-selected.png';
	  			}
	  			var markerlatlon = new google.maps.LatLng(locations2[i]["Lat"], locations2[i]["Lon"]);
	  			var typeclass;
	  			var computeddistance = google.maps.geometry.spherical.computeDistanceBetween(markerlatlon, location);
        		var distancemiles = computeddistance*0.000621371;
 				
        		if (distancemiles <= 50) {
        			marker = new google.maps.Marker({
			            position: markerlatlon,
			            map: map,
			            icon: markerimg,
			        });

			        var listviewTypeClass = '';
			        var listviewTypeName = '';
			        var listviewButtonClass = '';
			        if (locations2[i]['WG'] == 'I') { 
			        	listviewTypeClass = 'rc-inspection';
			        	listviewTypeName = 'Inspection';
			        	listviewButtonClass = 'rc-blue-btn';
			        } else if (locations2[i]['WG'] == 'G') { 
			        	listviewTypeClass = 'rc-lawn';
			        	listviewTypeName = 'Lawn & Snow';
			        	listviewButtonClass = 'rc-green-btn';
			        } else if (locations2[i]['WG'] == 'M') { 
			        	listviewTypeClass = 'rc-maintenance';
			        	listviewTypeName = 'Maintenance';
			        	listviewButtonClass = 'rc-orange-btn';
			        }
			        var addressNoNumber = locations2[i]["A"];
			        addressNoNumber = / (.+)/.exec(addressNoNumber)[1];
		        
        			var listviewcontent = '<div class="rc-property-info '+listviewTypeClass+'" data-category="'+locations2[i]["WG"]+'" data-image="'+locations2[i]["I"]+'" data-pricing="'+locations2[i]["P"]+'" data-worktype="'+locations2[i]["WT"]+'" data-address="'+addressNoNumber+', '+locations2[i]["C"]+', '+locations2[i]["S"]+' '+locations[i]["Z"]+'">';
		        	var imageclass = '';
		        	listviewcontent += '<div class="rc-infowindow-header '+listviewTypeClass+'"><span class="rc-icon '+listviewTypeClass+'-icon"></span>'+listviewTypeName+'</div>';
		        	if (locations2[i]["I"] != '' && locations2[i]["I"] != null) { 
		        		imageclass = 'rc-house-image';
		        	}
		        	listviewcontent += '<div class="rc-list-view-info-inner '+imageclass+'">';
		        	if (locations2[i]["I"] != '' && locations2[i]["I"] != null) {
		        		listviewcontent += '<div class="rc-property-image"><img src="'+locations2[i]["I"]+'"></div>';
		        	}
		        	if (locations2[i]["WT"] != '' && locations2[i]["WT"] != null) {
		        		listviewcontent += '<div class="row rc-row"><strong>Work Type:</strong> '+locations2[i]["WT"]+'</div>';
		        	}
		        	listviewcontent += '<div class="row rc-row"><strong>Address:</strong> '+addressNoNumber+', '+locations2[i]["C"]+', '+locations2[i]["S"]+' '+locations[i]["Z"]+'</div>';
		        	if (locations2[i]["P"] != '' && locations2[i]["P"] != null) {
		        		listviewcontent += '<div class="row rc-row rc-pricing-row"><strong>Pricing: </strong> '+locations2[i]["P"]+'</div>';
		        	}
		        	listviewcontent += '<div class="row rc-row"><a class="rc-details-link" href="'+locations2[i]["T"]+'" target="_blank">Work Order Description ></a></div>';
		        	listviewcontent += '<div class="row rc-row rc-request-work-row"><a class="rc-request-work-link btn rc-btn '+listviewButtonClass+'" data-order="'+locations2[i]["O"]+'">Request Work</a></div>';
		        	listviewcontent += '</div>';
		        	listviewcontent += '</div>';
		        	$('.rc-list-view-container').append(listviewcontent);

			        google.maps.event.addListener(marker, 'click', (function (marker, i) {
			            return function () {
			            	$('.rc-work-order-drawer').fadeOut();
							if ($(window).width() < 600) {
								$('.rc-map-container .rc-sidebar .rc-btn.rc-filter-btn').fadeIn();
								$('.rc-sidebar-work-order-container').fadeOut();
							}
			            	var markerlatlon2 = new google.maps.LatLng(locations2[i]["Lat"], locations2[i]["Lon"]);
			            	if (locations2[i]['WG'] == 'I') {
				  				header = '<div class="rc-infowindow-header rc-inspection"><span class="rc-icon rc-inspection-icon"></span>Inspection <span class="rc-close-button-mobileinfo rc-mobile-only"></span></div>';
				  				typeclass="rc-inspection";
				  				oldicon = 'Images/inspection-dot.png';
	  							markerimgselected = 'Images/inspection-dot-selected.png';
				  			} else if (locations2[i]['WG'] == 'G') {
				  				header = '<div class="rc-infowindow-header rc-lawn"><span class="rc-icon rc-lawn-icon"></span>Lawn & Snow <span class="rc-close-button-mobileinfo rc-mobile-only"></span></div>';
				  				typeclass="rc-lawn";
				  				oldicon = 'Images/lawn-dot.png';
	  							markerimgselected = 'Images/lawn-dot-selected.png';
				  			} else if (locations2[i]['WG'] == 'M') {
				  				header = '<div class="rc-infowindow-header rc-maintenance"><span class="rc-icon rc-maintenance-icon"></span>Maintenance <span class="rc-close-button-mobileinfo rc-mobile-only"></span></div>';
				  				typeclass="rc-maintenance";
				  				oldicon = 'Images/maintenance-dot.png';
	  							markerimgselected = 'Images/maintenance-dot-selected.png';
				  			}
				  			var addressNoNumber2 = locations2[i]["A"];
			        		addressNoNumber2 = / (.+)/.exec(addressNoNumber2)[1];
			            	var content = '<div class="rc-info-content '+typeclass+'" data-category="'+locations2[i]["WG"]+'" data-image="'+locations2[i]["I"]+'" data-pricing="'+locations2[i]["P"]+'" data-worktype="'+locations2[i]["WT"]+'" data-address="'+addressNoNumber2+', '+locations2[i]["C"]+', '+locations2[i]["S"]+' '+locations[i]["Z"]+'" data-description-link="'+locations2[i]["T"]+'">'+header;
			            	if (locations2[i]["WT"] != null) {
			            		content += '<div class="row rc-row">'+locations2[i]["WT"]+'</div>';
			            	}
			            	content += '<div class="row rc-row">'+addressNoNumber2+', '+locations2[i]["C"]+', '+locations2[i]["S"]+' '+locations[i]["Z"]+'</div>';
			            	if (locations2[i]["P"] != null ) {
			            		content += '<div class="row rc-row">'+locations2[i]["P"]+'</div>';
			            	}
			            	if ($(window).width() > 600 ) {
			            		content += '<div class="row rc-row text-center"><a class="rc-details-link" data-order="'+locations2[i]['O']+'">Click For Details</a></div>';
			            	} else {
			            		content += '<div class="row rc-row text-center"><a class="rc-details-link" data-order="'+locations2[i]['O']+'">Tap For Details</a></div>';
			            	}
			            	content += '</div>';
			            	if ($(window).width() > 600 ) {
				            	SAFEGUARD.site.offsetCenter(map, markerlatlon2, -100, 0);
				            } else {
				            	SAFEGUARD.site.offsetCenter(map, markerlatlon2, 0, 0);
				            }
			            	if ($(window).width() > 600 ) {
			                	infowindow.setContent(content);
			                	infowindow.open(map, marker);
			                } else {
			                	$('.rc-infowindow-mobile').slideUp(200);
			                	setTimeout(function(){
				                	$('.rc-infowindow-mobile .rc-info-content').html(content);
				                	$('.rc-infowindow-mobile').slideDown();
				                	$('.rc-infowindow-mobile .rc-close-button-mobileinfo').click(function(){
										$('.rc-infowindow-mobile').slideUp();
										for (var j = 0; j < markers.length; j++) {
									  		if (markers[j]['icon'] == 'Images/inspection-dot-selected.png') {
								  				oldicon = 'Images/inspection-dot.png';
								  			} else if (markers[j]['icon'] == 'Images/lawn-dot-selected.png') {
								  				oldicon = 'Images/lawn-dot.png';
								  			} else if (markers[j]['icon'] == 'Images/maintenance-dot-selected.png') {
								  				oldicon = 'Images/maintenance-dot.png';
								  			} else if (markers[j]['icon'] == 'Images/inspection-dot.png') {
								  				oldicon = 'Images/inspection-dot.png';
								  			} else if (markers[j]['icon'] == 'Images/lawn-dot.png') {
								  				oldicon = 'Images/lawn-dot.png';
								  			} else if (markers[j]['icon'] == 'Images/maintenance-dot.png') {
								  				oldicon = 'Images/maintenance-dot.png';
								  			}
						                    markers[j].setIcon(oldicon);
						                }
									});
				                }, 201);
			                }
			                setTimeout(function(){
			                	SAFEGUARD.site.openDetailsInfoWindow();
			                	if ($(window).width() > 600) {
				                	$('.rc-work-order-drawer .rc-work-order-close-btn').click(function(){
				                		for (var j = 0; j < markers.length; j++) {
									  		if (markers[j]['icon'] == 'Images/inspection-dot-selected.png') {
								  				oldicon = 'Images/inspection-dot.png';
								  			} else if (markers[j]['icon'] == 'Images/lawn-dot-selected.png') {
								  				oldicon = 'Images/lawn-dot.png';
								  			} else if (markers[j]['icon'] == 'Images/maintenance-dot-selected.png') {
								  				oldicon = 'Images/maintenance-dot.png';
								  			} else if (markers[j]['icon'] == 'Images/inspection-dot.png') {
								  				oldicon = 'Images/inspection-dot.png';
								  			} else if (markers[j]['icon'] == 'Images/lawn-dot.png') {
								  				oldicon = 'Images/lawn-dot.png';
								  			} else if (markers[j]['icon'] == 'Images/maintenance-dot.png') {
								  				oldicon = 'Images/maintenance-dot.png';
								  			}
						                    markers[j].setIcon(oldicon);
						                }
				                	});
				                }
			                	$('.rc-info-content .rc-details-link').click(function(){
						    		infowindow.close();
								  	//$('.infowindow-mobile').slideUp(200);
						    	});
			                }, 500);
			                for (var j = 0; j < markers.length; j++) {
						  		if (markers[j]['icon'] == 'Images/inspection-dot-selected.png') {
					  				oldicon = 'Images/inspection-dot.png';
					  			} else if (markers[j]['icon'] == 'Images/lawn-dot-selected.png') {
					  				oldicon = 'Images/lawn-dot.png';
					  			} else if (markers[j]['icon'] == 'Images/maintenance-dot-selected.png') {
					  				oldicon = 'Images/maintenance-dot.png';
					  			} else if (markers[j]['icon'] == 'Images/inspection-dot.png') {
					  				oldicon = 'Images/inspection-dot.png';
					  			} else if (markers[j]['icon'] == 'Images/lawn-dot.png') {
					  				oldicon = 'Images/lawn-dot.png';
					  			} else if (markers[j]['icon'] == 'Images/maintenance-dot.png') {
					  				oldicon = 'Images/maintenance-dot.png';
					  			}
			                    markers[j].setIcon(oldicon);
			                }
			                marker.setIcon(markerimgselected);
			            };
			        })(marker, i));
			        
			        markers.push(marker);
		    	}

		        google.maps.event.addListener(map, "click", function () { 
				  	infowindow.close();
				  	$('.rc-infowindow-mobile').slideUp(200);

				  	for (var j = 0; j < markers.length; j++) {
				  		if (markers[j]['icon'] == 'Images/inspection-dot-selected.png') {
			  				oldicon = 'Images/inspection-dot.png';
			  			} else if (markers[j]['icon'] == 'Images/lawn-dot-selected.png') {
			  				oldicon = 'Images/lawn-dot.png';
			  			} else if (markers[j]['icon'] == 'Images/maintenance-dot-selected.png') {
			  				oldicon = 'Images/maintenance-dot.png';
			  			} else if (markers[j]['icon'] == 'Images/inspection-dot.png') {
			  				oldicon = 'Images/inspection-dot.png';
			  			} else if (markers[j]['icon'] == 'Images/lawn-dot.png') {
			  				oldicon = 'Images/lawn-dot.png';
			  			} else if (markers[j]['icon'] == 'Images/maintenance-dot.png') {
			  				oldicon = 'Images/maintenance-dot.png';
			  			}
	                    markers[j].setIcon(oldicon);
	                }
				});
		        $('.rc-list-view-container .rc-property-info .rc-request-work-link').click(function(e){
					e.preventDefault();
					var workordertosend = $(this).attr('data-order'); 
					SAFEGUARD.site.SubmitRequests(workordertosend);
				});
		    }
		},
	initAutocomplete: function() {
		var places;
		var autocomplete;
		var autocompletemobile;
		var autocompleteInterior;
		var autocompleteInteriorMobile;
 		var countryRestrict = {'country': 'us'};
		autocomplete = new google.maps.places.Autocomplete(
           (
                document.getElementById('autocomplete-search1')), {
              types: ['(regions)'],
              componentRestrictions: countryRestrict
            });
		autocompleteInterior = new google.maps.places.Autocomplete(
           (
                document.getElementById('sidebar-location-value1')), {
              types: ['(regions)'],
              componentRestrictions: countryRestrict
        });
        autocompleteInteriorMobile = new google.maps.places.Autocomplete(
           (
                document.getElementById('sidebar-location-value2')), {
              types: ['(regions)'],
              componentRestrictions: countryRestrict
        });
        // places = new google.maps.places.PlacesService(map);
        autocompleteInterior.addListener('place_changed', function(){
        	var newLocationValue = $('#sidebar-location-value1').val();
        	$('.rc-zoom-in-snackbar').hide();
        	SAFEGUARD.site.getLocationSearchLatLon(newLocationValue, 'all');
        });
        autocompleteInteriorMobile.addListener('place_changed', function(){
        	var newLocationValue = $('#sidebar-location-value2').val();
        	$('.rc-zoom-in-snackbar').hide();
        	$('.rc-infowindow-mobile').hide();
        	SAFEGUARD.site.getLocationSearchLatLon(newLocationValue, 'all');
        });

        autocomplete.addListener('place_changed', function(){
        	$('#browser-orders-submit').prop('disabled', false);
        });
	},
	ZoomControl: function(controlDiv, map) {
  
	  
	  controlDiv.style.padding = '5px';

	  var controlWrapper = document.createElement('div');
	  controlWrapper.style.backgroundColor = 'transparent';
	  controlWrapper.style.borderStyle = 'solid';
	  controlWrapper.style.borderColor = 'gray';
	  controlWrapper.style.borderWidth = '0';
	  controlWrapper.style.cursor = 'pointer';
	  controlWrapper.style.textAlign = 'center';
	  controlWrapper.style.width = '32px'; 
	  controlWrapper.style.height = '64px';
	  controlDiv.appendChild(controlWrapper);
	  
	  var zoomInButton = document.createElement('div');
	  zoomInButton.style.width = '30px'; 
	  zoomInButton.style.height = '30px';
	  zoomInButton.style.backgroundColor = '#515153';
	  zoomInButton.style.backgroundImage = 'url("Images/icon-zoom-in.png")';
	  zoomInButton.style.backgroundPosition = 'center center';
	  zoomInButton.style.backgroundRepeat = 'no-repeat';
	  zoomInButton.style.borderColor = 'white';
	  zoomInButton.style.borderStyle = 'solid';
	  zoomInButton.style.borderWidth = '2px';
	  zoomInButton.style.borderRadius = '5px';
	  zoomInButton.style.color = 'white';
	  zoomInButton.style.marginBottom = '5px';
	  controlWrapper.appendChild(zoomInButton);

	  var zoomOutButton = document.createElement('div');
	  zoomOutButton.style.width = '30px'; 
	  zoomOutButton.style.height = '30px';
	  zoomOutButton.style.backgroundColor = '#515153';
	  zoomOutButton.style.backgroundImage = 'url("Images/icon-zoom-out.png")';
	  zoomOutButton.style.backgroundPosition = 'center center';
	  zoomOutButton.style.backgroundRepeat = 'no-repeat';
	  zoomOutButton.style.borderColor = 'white';
	  zoomOutButton.style.borderStyle = 'solid';
	  zoomOutButton.style.borderWidth = '2px';
	  zoomOutButton.style.borderRadius = '5px';
	  zoomOutButton.style.color = 'white';
	  controlWrapper.appendChild(zoomOutButton);

	  google.maps.event.addDomListener(zoomInButton, 'click', function() {
	    map.setZoom(map.getZoom() + 1);
	    if (map.getZoom() > 9) {
	    	$('.rc-zoom-in-snackbar').fadeOut();
	    }
	  });
	    
	  google.maps.event.addDomListener(zoomOutButton, 'click', function() {
	    map.setZoom(map.getZoom() - 1);
	    console.log('ZoomOut: ' + map.getZoom() );
	    if (map.getZoom() <= 9) {
	    	console.log('map too zoomed out');
	    	$('.rc-zoom-in-snackbar').fadeIn();
	    } else {
	    	$('.rc-zoom-in-snackbar').fadeOut();
	    }
	  });  
	    
	},	
	mapStyleControl: function(controlDiv, map) {
		controlDiv.style.width = '130px';
		controlDiv.style.padding = '5px';
		var controlUI = document.createElement('div');
		controlUI.style.width = '50%';
		controlUI.style.display = 'inline-block';
  		controlUI.style.backgroundColor = '#515153';
	  	controlUI.style.border = '2px solid #fff';
		controlUI.style.borderRadius = '5px';
		controlUI.style.cursor = 'pointer';
		controlUI.style.marginBottom = '0';
		controlUI.style.textAlign = 'center';
		controlUI.title = 'Click to view Roadmap';
		controlDiv.appendChild(controlUI);

		var controlUI2 = document.createElement('div');
		controlUI2.style.width = '50%';
		controlUI2.style.display = 'inline-block';
  		controlUI2.style.backgroundColor = '#515153';
	  	controlUI2.style.border = '2px solid #fff';
		controlUI2.style.borderRadius = '5px';
		controlUI2.style.cursor = 'pointer';
		controlUI2.style.marginBottom = '0';
		controlUI2.style.textAlign = 'center';
		controlUI2.title = 'Click to view satellite';
		controlDiv.appendChild(controlUI2);

		var controlText = document.createElement('div');
		controlText.style.color = '#fff';
		controlText.style.fontFamily = 'montserratsemibold,Arial,sans-serif';
		controlText.style.fontSize = '10px';
		controlText.style.lineHeight = '20px';
		controlText.style.paddingLeft = '5px';
		controlText.style.paddingRight = '5px';
		controlText.innerHTML = 'Map';
		controlUI.appendChild(controlText);

		var controlText2 = document.createElement('div');
		controlText2.style.color = '#fff';
		controlText2.style.fontFamily = 'montserratlight,Arial,sans-serif';
		controlText2.style.fontSize = '10px';
		controlText2.style.lineHeight = '20px';
		controlText2.style.paddingLeft = '5px';
		controlText2.style.paddingRight = '5px';
		controlText2.innerHTML = 'Satellite';
		controlUI2.appendChild(controlText2);

		google.maps.event.addDomListener(controlUI, 'click', function(){
			map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
			controlText.style.fontFamily = 'montserratsemibold,Arial,sans-serif';
			controlText2.style.fontFamily = 'montserratlight,Arial,sans-serif';
		});
		google.maps.event.addDomListener(controlUI2, 'click', function(){
			map.setMapTypeId(google.maps.MapTypeId.SATELLITE);
			controlText2.style.fontFamily = 'montserratsemibold,Arial,sans-serif';
			controlText.style.fontFamily = 'montserratlight,Arial,sans-serif';

		});
	},
	offsetCenter: function(map, latlng, offsetx, offsety) {
	    var scale = Math.pow(2, map.getZoom());

	    var worldCoordinateCenter = map.getProjection().fromLatLngToPoint(latlng);
	    var pixelOffset = new google.maps.Point((offsetx/scale) || 0,(offsety/scale) ||0);

	    var worldCoordinateNewCenter = new google.maps.Point(
	        worldCoordinateCenter.x - pixelOffset.x,
	        worldCoordinateCenter.y + pixelOffset.y
	    );

	    var newCenter = map.getProjection().fromPointToLatLng(worldCoordinateNewCenter);

	    map.panTo(newCenter);

	},
	openDetailsInfoWindow: function() {
		$('.rc-info-content .rc-details-link').click(function(){
				var workorderID = $(this).attr('data-order');
				var category = $(this).parents('.rc-info-content').attr('data-category');
				var image = $(this).parents('.rc-info-content').attr('data-image');
				var pricing = $(this).parents('.rc-info-content').attr('data-pricing');
				var worktype = $(this).parents('.rc-info-content').attr('data-worktype');
				var address = $(this).parents('.rc-info-content').attr('data-address');
				var descriptionlink = $(this).parents('.rc-info-content').attr('data-description-link');

				if (category == "I") {
					$('.rc-work-order-drawer').removeClass('rc-maintenance').removeClass('rc-lawn').addClass('rc-inspection');
					$('.rc-work-order-drawer .rc-type-title').html('<span class="rc-icon"></span>Inspection');
					$('.rc-work-order-drawer .rc-btn.rc-btn-primary').removeClass('rc-green-btn').removeClass('rc-orange-btn').addClass('rc-blue-btn');
				} else if (category == "G") {
					$('.rc-work-order-drawer').removeClass('rc-maintenance').removeClass('rc-inspection').addClass('rc-lawn');
					$('.rc-work-order-drawer .rc-type-title').html('<span class="rc-icon"></span>Lawn & Snow');
					$('.rc-work-order-drawer .rc-btn.rc-btn-primary').removeClass('rc-blue-btn').removeClass('rc-orange-btn').addClass('rc-green-btn');
				} else if (category == "M") {
					$('.rc-work-order-drawer').removeClass('rc-inspection').removeClass('rc-lawn').addClass('rc-maintenance');
					$('.rc-work-order-drawer .rc-type-title').html('<span class="rc-icon"></span>Maintenance');
					$('.rc-work-order-drawer .rc-btn.rc-btn-primary').removeClass('rc-green-btn').removeClass('rc-blue-btn').addClass('rc-orange-btn');
				}
				$('.rc-work-order-drawer .rc-btn.rc-btn-primary').attr('data-order', workorderID);

				if (image != '' && image != null && image != 'null') {
					$('.rc-work-order-drawer .rc-image').show();
					$('.rc-work-order-drawer .rc-image img').attr('src', image);
				} else {
					$('.rc-work-order-drawer .rc-image').hide();
					$('.rc-work-order-drawer .rc-image img').attr('src', '');
				}

				$('.rc-work-order-drawer .rc-data-rows').html('');
				if (worktype != '' && worktype != null) {
					$('.rc-work-order-drawer .rc-data-rows').append('<div class="row rc-row rc-row-label">Work Type:</div><div class="row rc-row">'+worktype+'</div>');
				}
				if (address != '' && address != null) {
					$('.rc-work-order-drawer .rc-data-rows').append('<div class="row rc-row rc-row-label">Address:</div><div class="row rc-row">'+address+'</div>');
				}
				if (pricing != '' && pricing != null) {
					$('.rc-work-order-drawer .rc-data-rows').append('<div class="row rc-row rc-row-label">Pricing:</div><div class="row rc-row">'+pricing+'</div>');
				}
				$('.rc-work-order-drawer .rc-data-rows').append('<div class="row rc-row rc-row-label rc-colored"><a href="'+descriptionlink+'" target="_blank">Work Order Description</a></div><div class="row rc-row"></div>');

				if ($(window).width() < 600) {
					$('.rc-sidebar-work-order-container').fadeIn();
					$('.rc-map-container .rc-sidebar .rc-btn.rc-filter-btn').fadeOut();
				}
				$('.rc-map-container .rc-work-order-drawer').fadeIn();
				
			});

	},
	mapFilter: function(){
		$('.rc-filter-btn').click(function(){

			var currentZoomLevel = map.getZoom();
			var currentMapCenter = map.getCenter();
			if ($(window).width() > 600) {
				var currentsearchval = $('#sidebar-location-value1').val();
			} else {
				var currentsearchval = $('#sidebar-location-value2').val();
			}
			var passFilterVar = 'all';
			$(this).toggleClass('rc-turned-off');
			if ($(this).parents('.rc-sidebar').find('.rc-filter-btn.rc-turned-off').length == 0) {
				passFilterVar = 'all';
			} else if ($(this).parents('.rc-sidebar').find('.rc-filter-btn.rc-turned-off').length < 3) {
				passFilterVar = '';
				if (!$(this).parents('.rc-sidebar').find('.rc-filter-btn[data-filter="G"]').hasClass('rc-turned-off')) {
					passFilterVar = 'G';
				}
				if (!$(this).parents('.rc-sidebar').find('.rc-filter-btn[data-filter="I"]').hasClass('rc-turned-off')) {
					passFilterVar = passFilterVar+'I';
				} 
				if (!$(this).parents('.rc-sidebar').find('.rc-filter-btn[data-filter="M"]').hasClass('rc-turned-off')) {
					passFilterVar = passFilterVar+'M';
				}
				
			} else {
				passFilterVar = 'none';
			}
			$('.rc-infowindow-mobile').slideUp(200);
			SAFEGUARD.site.getLocationSearchLatLon(currentsearchval, passFilterVar, currentZoomLevel, currentMapCenter);
		});
	},
	getLocationSearchLatLon: function(locationString, type, zoom, center){
		var geocoder = new google.maps.Geocoder();
		var location = locationString; //"Cleveland, OH";
		var latlon;
		geocoder.geocode( { 'address': location }, function(results, status) {
		    if (status == google.maps.GeocoderStatus.OK) {
		        latlon = results[0].geometry.location;
		        SAFEGUARD.site.googleMapsInitialize2(latlon, type, zoom, center);
		    } else {
		        console.log("Could not find location: " + location);
		    }
		});
	},
	mobileSwipeIntroScreens: function(){
		$(".rc-modal-step").swipe({
			swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
				$(this).find('.rc-status-bar li.rc-active').next().trigger('click');
			},
		});
		$(".rc-modal-step-5").swipe({
			swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
				$(this).find('.rc-btn.rc-btn-primary').trigger('click');
			},
		});
	},
	listViewTrigger: function() {
		$('.rc-list-view-btn').click(function(){
			var currentZoomLevel = map.getZoom();
			var currentMapCenter = map.getCenter();
			var thisBtn = $(this);
			if ($(window).width() > 600 ) {
				var currentsearchval = $('#sidebar-location-value1').val();
			} else {
				var currentsearchval = $('#sidebar-location-value2').val();
			}
			var passFilterVar = 'all';
			$(this).toggleClass('rc-turned-off');
			if ($(this).parents('.rc-sidebar').find('.rc-filter-btn.rc-turned-off').length == 0) {
				passFilterVar = 'all';
			} else if ($(this).parents('.rc-sidebar').find('.rc-filter-btn.rc-turned-off').length < 3) {
				passFilterVar = '';
				if (!$(this).parents('.rc-sidebar').find('.rc-filter-btn[data-filter="G"]').hasClass('rc-turned-off')) {
					passFilterVar = 'G';
				}
				if (!$(this).parents('.rc-sidebar').find('.rc-filter-btn[data-filter="I"]').hasClass('rc-turned-off')) {
					passFilterVar = passFilterVar+'I';
				} 
				if (!$(this).parents('.rc-sidebar').find('.rc-filter-btn[data-filter="M"]').hasClass('rc-turned-off')) {
					passFilterVar = passFilterVar+'M';
				}
				
			} else {
				passFilterVar = 'none';
			}

			if ($('#map-canvas').is(':visible')) {
				thisBtn.text('Go To Map View');
				$('#map-canvas').fadeOut(100);
				$('.rc-sidebar.rc-mobile-only .rc-list-view-info').removeClass('rc-list-icon').addClass('rc-map-icon');
				setTimeout(function(){
					$('.rc-work-order-drawer').fadeOut();
					if ($(window).width() < 600) {
						$('.rc-sidebar-work-order-container').fadeOut();
					}
					$('#list-view').fadeIn(400);
					$('.rc-scroll-alert-bar').fadeIn(400);
					$('.rc-infowindow-mobile').hide();
				}, 101);
			} else {
				thisBtn.text('Go To List View');
				$('.rc-sidebar.rc-mobile-only .rc-list-view-info').removeClass('rc-map-icon').addClass('rc-list-icon');
				setTimeout(function(){
					$('#map-canvas').fadeIn(400);
				}, 101);
				$('#list-view').fadeOut(100);
				$('.rc-scroll-alert-bar').fadeOut(100);
				setTimeout(function(){
					SAFEGUARD.site.getLocationSearchLatLon(currentsearchval, passFilterVar, currentZoomLevel, currentMapCenter);
				}, 601);
			}
			
			$('#list-view').scroll(function() {
				var listScrollPosition = $('#list-view').scrollTop();
			   	var containerHeight = $('#list-view').height();
			   	var innerHeight = $('.rc-list-view-container').height();

			   	if (listScrollPosition >= (innerHeight - containerHeight + 28)) {
			   		$('.rc-scroll-alert-bar').fadeOut();
			   	} else {
			   		$('.rc-scroll-alert-bar').fadeIn();
			   	}
			});
			if ($(window).width() < 600) {
				$(window).scroll(function() {
					var listScrollPosition = $('body').scrollTop();
				   	var containerHeight = $('body').height();
				   	var innerHeight = $('.rc-list-view-container').height();
				   	console.log(listScrollPosition + ' and '+(innerHeight - containerHeight + 28));
				   	if (listScrollPosition >= (innerHeight - containerHeight + 28)) {
				   		//console.log(listScrollPosition + ' and '+(innerHeight - containerHeight + 28));
				   		$('.rc-scroll-alert-bar').fadeOut();
				   	} else {
				   		$('.rc-scroll-alert-bar').fadeIn();
				   	}
				});
			}
		});
	},
	SubmitRequests: function(workOrder) {
		// ***** WILL HAVE TO MODIFY THIS CODE BASED ON WHAT DATA IS AVAILABLE ***** //

			// THIS WILL SEND THE SUBMISSION DATA TO WHEREVER IT NEEDS SENT - MIGHT NEED TO PASS MORE USER VARIABLES
			$.ajax({
			  method: "POST",
			  url: "some_url_or_file_for_submission",
			  data: {workorder: workOrder},
			}).done(function(){
				//TRIGGERS SUCCESS MODAL
				$('.rc-modal.rc-modal-submit-success').modal({
					backdrop: 'static',
				});
				$('.rc-modal.rc-modal-submit-success').on('shown.bs.modal', function(e){
					$('.rc-map-container').animate({'opacity': '0'});
				});
				$('.rc-modal.rc-modal-submit-success').on('hide.bs.modal', function(e){
					if ($(window).width() > 600 ) {
						var currentsearchval = $('#sidebar-location-value1').val();
					} else {
						var currentsearchval = $('#sidebar-location-value2').val();
					}
					//RE-INITIATES MAP SO THAT UPDATED POINTS ARE PULLED
					SAFEGUARD.site.getLocationSearchLatLon(currentsearchval);
		        	$('.rc-map-container').animate({'opacity': '1'});
				});

			}).fail(function(){
				// TRIGGERS UNAUTHORIZED MODAL
				$('.rc-modal.rc-modal-submit-fail').modal({
					backdrop: 'static',
				});
				$('.rc-modal.rc-modal-submit-fail').on('shown.bs.modal', function(e){
					$('.rc-map-container').animate({'opacity': '0'});
				});
				$('.rc-modal.rc-modal-submit-fail').on('hide.bs.modal', function(e){
		        	$('.rc-map-container').animate({'opacity': '1'});
				});

			}).always(function(){

			});
	},
	recenterMap: function() {
		if ($(window).width() > 600 ) {
			var newLocationValue = $('#sidebar-location-value1').val();
		} else {
			var newLocationValue = $('#sidebar-location-value2').val();
		}
    	$('.rc-zoom-in-snackbar').hide();
    	$('.rc-infowindow-mobile').hide();
    	SAFEGUARD.site.getLocationSearchLatLon(newLocationValue, 'all');
	},
	jsonLocations: function(){
		// ***** POINT TO WHICHEVER PATH JSON WILL BE ***** //
		$.getJSON( "/Scripts/Controls/MarketplaceMap/locations.json", function( data ) {
			$.each(data, function(key, val){
				locations.push(val);
			});
		}).done(function(){ });
	},

};
}(); 
var $ = jQuery.noConflict();
jQuery(document).ready(function($){
	SAFEGUARD.site.init();
});